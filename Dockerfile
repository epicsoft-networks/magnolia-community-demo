FROM registry.gitlab.com/epicdocker/java:jre11.0.18

ENV MAGNOLIA_VERSION 6.2.28
ENV TOMCAT_VERSION 9.0.71
ENV CATALINA_HOME /usr/local/tomcat
ENV CATALINA_TMPDIR /tmp/catalina
ENV PATH $CATALINA_HOME/bin:$PATH

ARG BUILD_DATE="development"
LABEL org.label-schema.name="magnolia-community-demo" \
      org.label-schema.description="Magnolia Community webapp with the travel demo" \
      org.label-schema.vendor="epicsoft LLC / Alexander Schwarz <as@epicsoft.one>" \
      org.label-schema.version="$MAGNOLIA_VERSION" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=${BUILD_DATE} \
      org.label-schema.url="https://gitlab.com/epicsoft-networks/magnolia-community-demo" \
      org.label-schema.vcs-url="https://gitlab.com/epicsoft-networks/magnolia-community-demo/tree/main"

LABEL image.name="magnolia-community-demo" \
      image.description="Magnolia Community webapp with the travel demo" \
      maintainer="epicsoft LLC" \
      maintainer.name="Alexander Schwarz <as@epicsoft.one>" \
      maintainer.copyright="Copyright 2023 epicsoft LLC / Alexander Schwarz" \
      license="MIT" \
      magnolia_version="$MAGNOLIA_VERSION" \
      tomcat_version="$TOMCAT_VERSION"

WORKDIR $CATALINA_HOME

RUN mkdir -p "$CATALINA_HOME" "$CATALINA_TMPDIR"

COPY "magnolia-$MAGNOLIA_VERSION/apache-tomcat-$TOMCAT_VERSION" "$CATALINA_HOME"

RUN chmod +x $CATALINA_HOME/bin/*.sh

EXPOSE 8080

CMD ["catalina.sh", "run"]
