# Magnolia Community webapp with the travel demo

## Run

### Just try

```bash
docker run --rm -it --name magnolia-demo \
  --mount type=tmpfs,destination=/tmp/catalina \
  -p 8080:8080 \
  registry.gitlab.com/epicsoft-networks/magnolia-community-demo:latest
```

### Detach

```bash
docker run -d --name magnolia-demo \
  --mount type=tmpfs,destination=/tmp/catalina \
  -p 8080:8080 \
  registry.gitlab.com/epicsoft-networks/magnolia-community-demo:latest
```

##  License

MIT License see [LICENSE](LICENSE)

Please note that the MIT license only applies to the files created by epicsoft LLC.
Other software may be licensed under other licenses.
